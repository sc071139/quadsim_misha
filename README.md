# quadsim_misha

##Description
**quadsim_misha** is a package for quadrotor simulation, based on ros system and gazebo simulation environment.

## Requirement
1. Ubuntu 16.04
2. Python2.7
3. ROS  ([installation tutorial](http://wiki.ros.org/kinetic/Installation/Ubuntu), install `ros-kinetic-desktop-full`)
4. Gazebo ([installation tutorial](http://gazebosim.org/tutorials?tut=ros_installing), begin with section `B. Install from Source (on Ubuntu)`)
5. A catkin workspace
	1. Source first (always..,): `source /opt/ros/kinetic/setup.bash`
	2. Create your catkin workspace (substitute '~/catkin_ws' with the path and dirname you want, but keep the "/src" part!): `mkdir -p ~/catkin_ws/src && cd ~/caktkin_ws/ && catkin_make`
	3. Source locally: `source devel/setup.bash`
	4. Check path: `echo $ROS_PACAKGE_PATH` and you should see your new created path with the older one
5. Clone from bitbucket
	1. Enter the "src" dir in this catkin workspace `cd ~/catkin_ws/src`
	2. Clone from remote repo `git clone https://sc071139@bitbucket.org/sc071139/quadsim_misha.git`

## Usage
0. `cd ~/catkin_ws/ && source devel/setup.bash && cd src/quadsim_misha`
1. `cd ./misha_gazebo/launch`
2. `roslaunch spawn_world.launch world:=levine`
3. `roslaunch spawn_robots.launch robot_type:=pelican`